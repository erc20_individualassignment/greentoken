// App object

App = {
    //object member variables
    web3Provider: null,//store web3 reference
    contracts: {},//instantiate an empty object
    /*
    init : 1.Setup web3 provider,
    2.Load contracts,
    3,Binds events to html components
    */
    init: () => {
        // Initialise web3 and set the provider to the testRPC.
        if (!typeof web3 !== 'undefined') {
            App.web3Provider = window.ethereum;
            web3 = new Web3(window.ethereum);
        } else {

            // set the provider you want from Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
            web3 = new Web3(App.web3Provider);
        }

        //Next, we load the contracts
        return App.initContract();
    },
    initContract: () => {
        $.getJSON('GreenToken.json', (data) => {
            // Get the necessary contract artifact file and instantiate it with truffle-contract.
            var MyTokenArtifact = data;
            App.contracts.GreenToken = TruffleContract(MyTokenArtifact);
            // Set the provider for our contract.
            App.contracts.GreenToken.setProvider(App.web3Provider);
        });
        //Next, we bind the event handlers of html components
        return App.bindEvents();
    },
    bindEvents: () => {
        $(document).on('click', '#addButton', App.handleAddItem);
    },
    handleAddItem: (event) => {
        event.preventDefault();
        //Retrieve the values from the input box
        var itemName = $('#itemName').val();
        var itemImg = $('#itemImage').val();
        var itemDescription = $('#itemDescription').val();
        var amount = parseInt($('#itemPrice').val());
        console.log('Transfer ' + amount + ' itemName: ' + itemName+ 'itemImg: '+itemImg+ 'itemDescription: '+itemDescription);
        var myTokenInstance;
        //Get the accounts
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            App.contracts.GreenToken.deployed().then(function (instance) {
                myTokenInstance = instance;
                //call the contract transfer method to execute the transfer
                return myTokenInstance.addItem(itemName, itemDescription,itemImg,amount, { from: account});
            }).then(function (result) {
                alert('Item Added Successful!');
                resetform();
            }).catch(function (err) {
                console.log(err.message);
            });
        });
    }
},
// Web page loaded event handler
$(() => {
    $(window).load(() => {
        App.init();
    });
});
function resetform() {
    document.getElementById("itemName").value = "";
    document.getElementById("itemImage").value = "";
    document.getElementById("itemDescription").value = "";
    document.getElementById("itemPrice").value = "";
}