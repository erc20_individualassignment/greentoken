const App = {
    web3Provider: null,
    contracts: {},
    account: null,
  
    init: async () => {
        if (typeof window.ethereum !== 'undefined') {
            App.web3Provider = window.ethereum;
            web3 = new Web3(window.ethereum);
            await window.ethereum.enable();
        } else {
            App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7111');
            web3 = new Web3(App.web3Provider);
        }
  
        await App.initContract();
        await App.getUserBoughtItems();
        
    },
  
    initContract: async () => {
        try {
            const response = await fetch('GreenToken.json');
            if (!response.ok) {
                throw new Error(`Failed to fetch Token.json. Status: ${response.status}`);
            }
  
            const data = await response.json();
            const MyTokenArtifact = data;
            App.contracts.GreenToken = TruffleContract(MyTokenArtifact);
            App.contracts.GreenToken.setProvider(App.web3Provider);
        } catch (err) {
            console.error('Error initializing contract:', err);
        }
    },
  
  getUserBoughtItems: async () => {
    try {
      const contractInstance = await App.contracts.GreenToken.deployed();
        // Get the user's Ethereum account
    const accounts = await web3.eth.getAccounts();
    const userAccount = accounts[0];
      const userBoughtItems = await contractInstance.getUserItems(userAccount);
        console.log(userBoughtItems[0]);
      // Assuming you have a DOM element with the id 'userBoughtItemsList' to display the items
      const boughtItemsList = document.querySelector('.userBoughtItemsList');
      
      // Assuming you have a MetaMask address stored in a variable called 'metamaskAddress'
    const metamaskAddress = userAccount;

    // Find the HTML element with the id "metamask"
    const metamaskElement = document.getElementById("metamask");

    // Set the content of the element to display the MetaMask address
    metamaskElement.innerHTML = `<strong>Metamask Address:</strong> ${metamaskAddress}`;
      // Clear the existing content
      boughtItemsList.innerHTML = '';
      // Loop through the user's bought items and display them
      userBoughtItems.forEach((item) => {
        // You can format and display the item details as you prefer
        const itemElement = document.createElement('div');
        itemElement.innerHTML = `
            <div class="contents-img">
                      <img src="./img/solarpanel.jpg" alt="" class=img-solar>
            </div>
          <p class="name">Item Name: ${item.itemName}</p>
          <p class="description">Item Description: ${item.itemDescription}</p>
          <hr class="line">
        `;
        boughtItemsList.appendChild(itemElement);
      });
    } catch (error) {
      console.error('Error fetching user\'s bought items:', error);
    }
  },
  
};
$(() => {
  $(window).load(() => {
    App.init();
  });
});
