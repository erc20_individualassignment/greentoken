const App = {
  web3Provider: null,
  contracts: {},
  account: null,

  init: async () => {
      if (typeof window.ethereum !== 'undefined') {
          App.web3Provider = window.ethereum;
          web3 = new Web3(window.ethereum);
          await window.ethereum.enable();
      } else {
          App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7111');
          web3 = new Web3(App.web3Provider);
      }

      await App.initContract();
      await App.loadData();
      App.bindEvents();
      
  },

  initContract: async () => {
      try {
          const response = await fetch('GreenToken.json');
          if (!response.ok) {
              throw new Error(`Failed to fetch Token.json. Status: ${response.status}`);
          }

          const data = await response.json();
          const MyTokenArtifact = data;
          App.contracts.GreenToken = TruffleContract(MyTokenArtifact);
          App.contracts.GreenToken.setProvider(App.web3Provider);
      } catch (err) {
          console.error('Error initializing contract:', err);
      }
  },

  loadData: async () => {
      try {
          const contractInstance = await App.contracts.GreenToken.deployed();
          // Call the contract's getNumDonationPlans method to get the number of donation plans
          const itemCount = await contractInstance.getItemsCount();
       
          // Initialize an array to store the donation plan data
          const BuyPlanData = [];

          // Loop through each donation plan and retrieve its data
          for (let i = 0; i < itemCount; i++) {
              const plan = await contractInstance.getItem(i);

              BuyPlanData.push({
                  itemName: plan[0],
                  itemDescription: plan[1],
                  itemImageURI: plan[2],
                  price: plan[3],
              });
          }

          const cardcontainer = document.querySelector(".contents-card-container");
          var count = 0;
          BuyPlanData.forEach((GreenData) => {

              // Create a new card element
              const card = document.createElement("div");
              card.className = "contents-card";

              // Create the HTML content for the card
              card.innerHTML = `
                  <div class="contents-cards-image">
                      <img src="./img/solarpanel.jpg" alt="">
                  </div>
                  <div class="contents-card-content">
                  <div  class="array-index" style="display:none;">
                      ${count}
                      </div>
                  <div>
                      <div class="contents-card-header">
                      Item Image: ${GreenData.itemImageURI}
                      </div>
                      <div>
                      Item Name: ${GreenData.itemName}
                      </div>
                      <div>
                        Item Description: ${GreenData.itemDescription}
                      </div>
                      <div class="price">
                        <div>
                        <span class="pricenum">Price:<span> <span class="dollar">${GreenData.price} GET</span>
                        </div>
                      </div>
                      <button type="button" class="btn btn-info text-light" data-bs-toggle="modal"
                          data-bs-target="#exampleModal" data-bs-whatever="@mdo" id="buyButton">Buy</button>
                  </div>
              `;
              count++;
              // Append the card to the container
              cardcontainer.appendChild(card);
          });
      } catch (error) {
          console.error('Error loading charity data:', error);
      }
  },
  buyData: async (itemIndex) => {
    try {
      // Get the GreenToken instance
      
      const contractInstance = await App.contracts.GreenToken.deployed();
     
  
      // Check if the item is already bought
      const item = await contractInstance.items(itemIndex); 


     // Get the user's Ethereum account
    const accounts = await web3.eth.getAccounts();
    const userAccount = accounts[0]; // Assuming you want the first account
   
      // Check if the user has a sufficient balance
      const userBalance = await contractInstance.balanceOf(userAccount);
      const itemPrice = item.price;
    
  
      if (userBalance.words[0] < itemPrice.words[0]) {
        Swal.fire(
          'Error!',
          'Insufficient balance to buy this item.',
          'error'
        );
      }
      // Proceed with the purchase since the user has sufficient balance
      await contractInstance.buyItem(itemIndex, { from: userAccount });
  
      // Display a success pop-up message
      Swal.fire(
        'Bought Successful!',
        '',
        'success'
      ).then(function () {
        setTimeout(function () {
          window.location.reload(); 
        }, 100); 
            });
  
      console.log('Bought successful');
    } catch (error) {
      console.error('Error buying:', error.message);
    };
  },

  bindEvents: () => {
    $(document).on('click', '#buyButton', function () {
      const itemIndex = parseInt($(this).closest(".contents-card").find(".array-index").text());
      App.buyData(itemIndex);
    });
  },
};
// Web page loaded event handler
$(() => {
  $(window).on('load', () => {
      App.init();
  });
});