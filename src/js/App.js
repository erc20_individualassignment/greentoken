// App object

App = {
    //object member variables
    web3Provider: null,//store web3 reference
    contracts: {},//instantiate an empty object
    /*
    init : 1.Setup web3 provider,
    2.Load contracts,
    3,Binds events to html components
    */
    init: () => {
        // Initialise web3 and set the provider to the testRPC.
        if (!typeof web3 !== 'undefined') {
            App.web3Provider = window.ethereum;
            web3 = new Web3(window.ethereum);
        } else {

            // set the provider you want from Web3.providers
            App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
            web3 = new Web3(App.web3Provider);
        }

        //Next, we load the contracts
        return App.initContract();
    },
    initContract: () => {
        $.getJSON('GreenToken.json', (data) => {
            // Get the necessary contract artifact file and instantiate it with truffle-contract.
            var MyTokenArtifact = data;
            App.contracts.GreenToken = TruffleContract(MyTokenArtifact);
            // Set the provider for our contract.
            App.contracts.GreenToken.setProvider(App.web3Provider);
            // Use our contract to retrieve and mark the adopted pets.
            return App.getBalances();
        });
        //Next, we bind the event handlers of html components
        return App.bindEvents();
    },
    bindEvents: () => {
        $(document).on('click', '#transferButton', App.handleTransfer);
    },
    handleTransfer: (event) => {
        event.preventDefault();
        //Retrieve the values from the input box
        var amount = parseInt($('#MyTokenTransferAmount').val());
        var toAddress = $('#MyTokenTransferAddress').val();
        console.log('Transfer ' + amount + ' token to ' + toAddress);
        var myTokenInstance;
        //Get the accounts
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                console.log(error);
            }
            var account = accounts[0];
            App.contracts.GreenToken.deployed().then(function (instance) {
                myTokenInstance = instance;
                //call the contract transfer method to execute the transfer
                return myTokenInstance.transfer(toAddress, amount, { from: account, gas: 100000 });
            }).then(function (result) {
                alert('Transfer Successful!');
                resetform();
                return App.getBalances();
            }).catch(function (err) {
                console.log(err.message);
            });
        });
    },
    getBalances: () => {
        var myTokenInstance;
        /*Retrieve all the accounts that is currently connected
        to the blockchain*/
        web3.eth.getAccounts((error, accounts) => {
            if (error) console.log(error);
            //Use the first account
            var account = accounts[0];
            //Display the wallet address in the place holder
            $('#MyTokenWallet').text(account)

            //Get the reference of the deployed token in the blockchain
            App.contracts.GreenToken.deployed().then((instance) => {
                myTokenInstance = instance;
                //call the balanceOf the token of an account
                return myTokenInstance.balanceOf(account);
            }).then((result) => {
                console.log(result.words[0])
                balance = result.words[0];
                //Display the balance in the place holder
                $('#MyTokenBalance').text(balance);
             
            }).catch((err) => {
                console.log(err.message);
            });
        });
    },
}
// Web page loaded event handler
$(() => {
    $(window).load(() => {
        App.init();
    });
});
function resetform() {
    document.getElementById("MyTokenTransferAddress").value = "";
    document.getElementById("MyTokenTransferAmount").value = "";
}
