const GreenToken = artifacts.require("GreenToken");

contract("GreenToken", (accounts) => {
  let greenToken;

  before(async () => {
    greenToken = await GreenToken.deployed();
  });

  it("should have the correct name, symbol, and decimals", async () => {
    const name = await greenToken.name();
    const symbol = await greenToken.symbol();
    const decimals = await greenToken.decimals();

    assert.equal(name, "GreenToken", "Name is incorrect");
    assert.equal(symbol, "GET", "Symbol is incorrect");
    assert.equal(decimals.toNumber(), 2, "Decimals is incorrect");
  });

  it("should have an initial supply of 1000000 GET tokens", async () => {
    const totalSupply = await greenToken.totalSupply();

    assert.equal(
      totalSupply.toNumber(),
      10000000,
      "Initial supply is incorrect"
    );
  });

  it("should allow the admin to add items", async () => {
    await greenToken.addItem("Item 1", "Description 1", "URI 1", 100);
    await greenToken.addItem("Item 2", "Description 2", "URI 2", 200);

    const itemCount = await greenToken.getItemsCount();

    assert.equal(itemCount.toNumber(), 2, "Item count is incorrect");
  });
});