// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.9.0;
pragma experimental ABIEncoderV2;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title Green Eco Token Contract
 */
contract GreenToken is ERC20{
    string public name = "GreenToken";
    string public description = "A Token For promoting Green";
    string public symbol = "GET";

    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;

    struct Item {
        string itemName;
        string itemDescription;
        string itemImageURI; 
        uint256 price;
        address buyer;
    } 

    Item[] public items; // Store items in an array

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }

    // Function for the admin to add items
    function addItem(string memory _itemName, string memory _itemDescription, string memory _itemImageURI, uint256 _price) public {
        items.push(Item({
            itemName: _itemName,
            itemDescription: _itemDescription,
            itemImageURI: _itemImageURI,
            price: _price,
            buyer: address(0) 
        }));
    }

    // Function to retrieve all items
    function getItemsCount() public view returns (uint256) {
        return items.length;
    }

    // Function to retrieve a specific item by its index
    function getItem(uint256 index) public view returns (Item memory) {
        require(index < items.length, "Item index out of bounds");
        return items[index];
    }

    // Function to allow users to buy items using the token
    function buyItem(uint256 itemIndex) public {
        require(itemIndex < items.length, "Item index out of bounds");
        Item storage item = items[itemIndex];
        require(item.buyer == address(0), "Item has already been bought");
        require(balanceOf(msg.sender) >= item.price, "Insufficient balance");

        // Transfer tokens from the buyer to the contract
        transfer(address(this), item.price);

        // Update the buyer of the item
        item.buyer = msg.sender;
    }

    // Function to retrieve user's bought items
    function getUserItems(address user) public view returns (Item[] memory) {
        uint256 itemCount = 0;
        for (uint256 i = 0; i < items.length; i++) {
            if (items[i].buyer == user) {
                itemCount++;
            }
        }

        Item[] memory userBoughtItems = new Item[](itemCount);
        itemCount = 0;
        for (uint256 i = 0; i < items.length; i++) {
            if (items[i].buyer == user) {
                userBoughtItems[itemCount] = items[i];
                itemCount++;
            }
        }

        return userBoughtItems;
    }
}
